package mataDra.dao.creatures;

import java.util.List;

import mataDra.dao.DAO;
import mataDra.entity.creatures.CreatureEntity.Attribute;
import mataDra.entity.creatures.CreatureEntity.BattleStateType;
import mataDra.entity.creatures.PlayerEntity;

public class PlayerDAO extends DAO<PlayerEntity> {

	@Override
	public List<PlayerEntity> registerAll() {
		// TODO 自動生成されたメソッド・スタブ
		setEntity(new PlayerEntity(0, "ダミー1", 500, 3, 50, 100, 1, null, BattleStateType.MOVABLE, Attribute.FIRE, 99, false, 1));
		setEntity(new PlayerEntity(1, "ポプ子", 500, 3, 50, 100, 1, null, BattleStateType.MOVABLE, Attribute.FIRE, 99, false, 1));
		setEntity(new PlayerEntity(2, "ピピ美", 500, 3, 50, 100, 1, null, BattleStateType.MOVABLE, Attribute.WATER, 99, false, 1));
		setEntity(new PlayerEntity(3, "もよもと", 500, 3, 50, 100, 1, null, BattleStateType.MOVABLE, Attribute.WATER, 99, false, 1));
		setEntity(new PlayerEntity(4, "トンヌラ", 500, 3, 50, 100, 1, null, BattleStateType.MOVABLE, Attribute.WOOD, 99, false, 1));

		return getEntity();
	}

}
