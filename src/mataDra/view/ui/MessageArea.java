package mataDra.view.ui;

import mataDra.logics.ui.MeshFrameLogic;
import mataDra.logics.ui.MessageLogic;
import mataDra.logics.ui.WaitLogic;

public class MessageArea {
    /**
     * メッセージを表示する
     *
     * @param text
     * @param waitNum
     */
    public void show(String text, int waitNum) {
        //メッセージ用インスタンス
        MessageLogic messageLogic = new MessageLogic();
        // 外枠用インスタンス
        MeshFrameLogic frameLogic = new MeshFrameLogic();
        // ウェイト用インスタンス
        WaitLogic waitLogic = new WaitLogic();
        // メッセージテキストを1行ごとに分割して文字列リストに追加
        String[] textList = messageLogic.separate(text);

        // 外枠上線
        frameLogic.drawUpBorder(textList[0]);
        // 文字列一行ごとの処理
        for (String s : textList) {
            // 文字列を一文字ずつ全角文字に変換
            char[] chars = messageLogic.convert(s);
            // 外枠左線
            frameLogic.drawLeftBorder();
            // 空白以外の文字列を1文字ずつウェイトをかけて表示
            for (char c : chars) {

                System.out.print(c);
                if (c != '　') {
                    waitLogic.wait(waitNum);
                }
            }
            // 外枠右線
            frameLogic.drawRightBorder();

        }
        waitLogic.wait(3);
        // 外枠下線
        frameLogic.drawBottomBorder(textList[0]);
        System.out.println("\n");
    }

}
