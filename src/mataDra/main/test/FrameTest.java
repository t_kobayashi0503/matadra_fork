package mataDra.main.test;

import java.util.ArrayList;
import java.util.List;

import mataDra.logics.ui.BorderFrameLogic;
import mataDra.logics.ui.FrameLogic;
import mataDra.logics.ui.MeshFrameLogic;
import mataDra.logics.ui.StrongFrameLogic;
import mataDra.view.ui.FrameArea;

public class FrameTest {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		//フレームの種類をリストに格納
		List<FrameLogic> frames = new ArrayList<>();
		frames.add(new MeshFrameLogic());
		frames.add(new BorderFrameLogic());
		frames.add(new StrongFrameLogic());


		String text;

		//テキスト幅にあったフレームを表示
		FrameArea frameArea = new FrameArea();
		for(int i=0; i<frames.size();i++){
			text = "あいうえおかきくけこさしすせそたちつてと";
			frameArea.show(text, 0, frames.get(i));

			text = "名前：ミナト／HP：100／LEVEL：100";
			frameArea.show(text, 0, frames.get(i));

			text = "突然の死";
			frameArea.show(text, 0, frames.get(i));
		}



	}

}
